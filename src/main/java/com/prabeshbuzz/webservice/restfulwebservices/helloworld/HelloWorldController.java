package com.prabeshbuzz.webservice.restfulwebservices.helloworld;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {
	
	@GetMapping(path="/helloworld")
	public String helloWorld() {
		return "hello world";
	}
	
	@GetMapping(path="/helloworldbean")
	public HelloWorldBean helloWorldBean() {
		return new HelloWorldBean("hello world"); 
	}
	

	@GetMapping(path="/helloworldbean/{name}")
	public HelloWorldBean helloWorldBean(@PathVariable String name) {
		return new HelloWorldBean(name); 
	}
	
	@GetMapping(path="/helloworldbeanInternational")
	public String  helloWorldBeanInt() {
		return "Good Morning"; 
	}


}
